<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Student
Route::get('/student', 'StudentsController@index')
	->middleware('auth:web')
	->name('listStudent');

Route::get('/student/create', 'StudentsController@create')
	->middleware('auth:web')
	->name('createStudent');

Route::post('/student/create', 'StudentsController@create')
	->middleware('auth:web')
	->name('createStudent');	

Route::post('/student/update', 'StudentsController@update')
	->middleware('auth:web')
	->name('updateStudent');

Route::put('/student/edit', 'StudentsController@edit')
	->middleware('auth:web')
	->name('editStudent');
	
Route::delete('/student/destroy', 'StudentsController@destroy')
	->middleware('auth:web')
	->name('deleteStudent');

// Teacher
Route::get('/teacher', 'TeachersController@index')
	->middleware('auth:web')
	->name('listTeacher');

Route::get('/teacher/create', 'TeachersController@create')
	->middleware('auth:web')
	->name('createTeacher');

Route::post('/teacher/create', 'TeachersController@create')
	->middleware('auth:web')
	->name('createTeacher');	

Route::post('/teacher/edit', 'TeachersController@edit')
	->middleware('auth:web')
	->name('editTeacher');

Route::get('/teacher/edit', 'TeachersController@edit')
	->middleware('auth:web')
	->name('editTeacher');

Route::get('/teacher/destroy', 'TeachersController@destroy')
	->middleware('auth:web')
	->name('deleteTeacher');
	
// Class	
Route::get('/classes', 'ClassroomsController@index')
	->middleware('auth:web')
	->name('listClass');

Route::get('/classes/create', 'ClassroomsController@create')
	->middleware('auth:web')
	->name('createClass');

Route::post('/classes/create', 'ClassroomsController@create')
	->middleware('auth:web')
	->name('createClass');	

Route::get('/classes/edit', 'ClassroomsController@edit')
	->middleware('auth:web')
	->name('editClass');

Route::post('/classes/edit', 'ClassroomsController@edit')
	->middleware('auth:web')
	->name('editClass');

Route::get('/classes/destroy', 'ClassroomsController@destroy')
	->middleware('auth:web')
	->name('deleteClass');

Route::get('/classes/show', 'ClassroomsController@show')
	->middleware('auth:web')
	->name('showClass');

Route::post('/classes/show', 'ClassroomsController@show')
	->middleware('auth:web')
	->name('showClass');

Route::get('/classes/modify', 'ClassroomsController@modify')
	->middleware('auth:web')
	->name('modifyClass');

Route::get('/classes/update', 'ClassroomsController@update')
	->middleware('auth:web')
	->name('updateClass');

Route::get('/classes/removeStudent', 'ClassroomsController@removeStudent')
	->middleware('auth:web')
	->name('removeStudent');
	
	
Route::get('/', 'HomeController@index')
	->middleware('auth:web')
	->name('home');

Route::get('/home', 'HomeController@index')
	->middleware('auth:web')
	->name('home');
	