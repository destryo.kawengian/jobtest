@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@endif
            <div class="card">
                <div class="card-header">
					<div class="left">Class Detail</div>
					<div class="right"><a class="btn btn-primary" href="{{ route('showClass', ['id' => $class_id]) }}">{{ __('Back to Class') }}</a></div>
					<div class="clear"></div>
				</div>

                <div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							<thead>
							  <tr>
								<th><strong>Student Name</strong></th>
								<th><strong>Gender</strong></th>
								<th></th>
							  </tr>
							</thead>
							<tbody>
								@foreach($student_all as $val)
								<tr>
									<td>{{ $val->name}}</td>
									<td>{{ $val->sex}}</td>
									<td>
										<a class="btn btn-success" onclick="return confirm('Are you sure want to add?')"
											href="{{route('updateClass', ['student_id' => $val->id, 'class_id' => $class_id])}}">
											Add
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
	$('#student-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'https://datatables.yajrabox.com/collection/array-data',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'}
        ]
    });
</script>