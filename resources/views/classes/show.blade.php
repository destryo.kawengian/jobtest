@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@endif
            <div class="card">
                <div class="card-header">
					<div class="left">Class Detail</div>
					<div class="right"><a class="btn btn-primary" href="{{ route('modifyClass', ['id' => $data->id]) }}">{{ __('Add Student') }}</a></div>
					<div class="clear"></div>
				</div>

                <div class="card-body">
					<div class="row">
						<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Class Name') }}</label>
						<label for="name" class="col-md-4 col-form-label text-md-left">: {{ $data->name }}</label>
					</div>
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Teacher') }}</label>
						<label for="name" class="col-md-4 col-form-label text-md-left">: {{ $data->teacher->name }}</label>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							<thead>
							  <tr>
								<th><strong>Student Name</strong></th>
								<th><strong>Gender</strong></th>
								<th></th>
							  </tr>
							</thead>
							<tbody>
								@foreach($list_student as $val)
								<tr>
									<td>{{ $val->student->name}}</td>
									<td>{{ $val->student->sex}}</td>
									<td>
										<a class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')" 
											href="{{route('removeStudent', ['id' => $val->id, 'class_id' => $id])}}">
											Remove
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
	$('#student-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'https://datatables.yajrabox.com/collection/array-data',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'}
        ]
    });
</script>