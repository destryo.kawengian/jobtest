@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
			@endif
            <div class="card">
                <div class="card-header">
					<div class="left">List Student</div>
					<div class="right"><a class="btn btn-primary" href="{{ route('createStudent') }}">{{ __('Add') }}</a></div>
					<div class="clear"></div>
				</div>

                <div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							<thead>
							  <tr>
								<th><strong>Name</strong></th>
								<th><strong>Email</strong></th>
								<th><strong>Gender</strong></th>
								<th></th>
							  </tr>
							</thead>
							<tbody>
								@foreach($data as $val)
								<tr>
									<td>{{ $val->name }}</td>
									<td>{{ $val->email }}</td>
									<td>{{ $val->sex }}</td>
									<td>
                                        {{ Form::open(array('route' => 'editStudent', 'method' => 'PUT')) }}
										    <input type="hidden" name="_token" value="{{ csrf_token() }}">
										    <input type="hidden" value="<?php echo $val->id ?>" name="id" />
										    <button class="btn btn-success" >{{ __('Edit') }}</button>
                                        {{ Form::close() }}
                                        {{ Form::open(array('route' => 'deleteStudent', 'method' => 'DELETE')) }}
										    <input type="hidden" name="_token" value="{{ csrf_token() }}">
										    <input type="hidden" value="<?php echo $val->id ?>" name="id" />
										    <button onclick="return confirm('Are you sure want to delete?')" class="btn btn-danger" >{{ __('Delete') }}</button>
                                        {{ Form::close() }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
	$('#student-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'https://datatables.yajrabox.com/collection/array-data',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'}
        ]
    });
</script>