<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = Student::get();
		return view('student/index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request){
		$method = $request->method();
		if ($method == 'POST') {
			$student = new Student();
			
			$student->name = $request->input('name');
			$student->email = $request->input('email');
			$student->sex = $request->input('sex');
			
			$student->save();
			return redirect()->route('listStudent')->with('message', 'Data successfully inserted');
		} else {
			return view('student/create');			
		}

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request){
		
		$req = $request->toArray();
		$id = $req['id'];
		$data = Student::find($id);
		
		$selected = $data->sex;
		$dropdownList = array(
			'male' => 'MALE',
			'female' => 'FEMALE'
		);
		
		return view('student/edit', compact('data', 'selected', 'dropdownList'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
		$student_id = $request->input('id');
		
		$data_update = Student::find($student_id);

		$data_update->name = $request->input('name');
		$data_update->email = $request->input('email');
		$data_update->sex = $request->input('sex');
		
		$data_update->save();
		return redirect()->route('listStudent')->with('message', 'Data successfully updated');			
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
		$req = $request->toArray();
		$id = $req['id'];

		$data = Student::find($id);
		$data->delete();
		return redirect()->route('listStudent')->with('message', 'Data successfully deleted');
		//$request->delete();

        //
    }
}
