<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Classroom;
use App\Teacher;
use App\ClassroomDetail;
use App\Student;

class ClassroomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = Classroom::all();
		//dd($data);
		return view('Classes/index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request){
		$method = $request->method();
		$listTeacher = array();
		if ($method == 'POST') {
			$Classes = new Classroom();
			
			$Classes->name = $request->input('name');
			$Classes->teacher_id = $request->input('teacher_id');
			
			$Classes->save();
			return redirect()->route('listClass')->with('message', 'Data successfully inserted');
		} else {
			$data = Teacher::all();
			$listTeacher = $data;
		}
		return view('Classes/create', compact('listTeacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(request $request) {
		$id = $request->query('id');
		$data = Classroom::find($id);
		$list_student = ClassroomDetail::where('classroom_id', $id)->get();
		
		return view('Classes/show', compact('data', 'list_student', 'id'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request){
		
		$method = $request->method();
		
		if ($method == 'POST') {
			$Classes_id = $request->input('id');
			
			$data_update = Classroom::find($Classes_id);

			$data_update->name = $request->input('name');
			$data_update->teacher_id = $request->input('teacher_id');
			
			$data_update->save();
			return redirect()->route('listClass')->with('message', 'Data successfully updated');			
		} else {
			$id = $request->query('id');
			$data = Classroom::find($id);
			$dataAll = Teacher::all();
			$selected = $data->teacher_id;
			$listTeacher = $dataAll;
		}
		
		return view('Classes/edit', compact('data', 'selected', 'listTeacher'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modify(Request $request)
    {
		$class_id = $request->query('id');
		$student_all = array();
		
		$student_in_class = ClassroomDetail::where('classroom_id', $class_id)->get();
		$student_all = Student::get();
				
		return view('Classes/modify', compact('class_id', 'student_in_class', 'student_all'));
    }

    public function update(Request $request)
    {
		$student_id = $request->query('student_id');
		$class_id = $request->query('class_id');
		
		$in_class = DB::table('classroom_details')
			->where('classroom_id', '=', $class_id)
			->where('student_id', '=', $student_id)
			->count();
		
		if ($in_class == 0) {
			$Classroom = new ClassroomDetail();
			
			$Classroom->student_id = $student_id;
			$Classroom->classroom_id = $class_id;
			
			$Classroom->save();
			return redirect()->route('modifyClass', ['id' => $class_id])->with('message', 'Student successfully added');			
		} else {
			return redirect()->route('modifyClass', ['id' => $class_id])->with('message', 'The Student already in class');
		}
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
		$id = $request->query('id');
		$data = Classroom::find($id);
		$data->delete();
		return redirect()->route('listClass')->with('message', 'Data successfully deleted');
		//$request->delete();

        //
    }
	
	public function removeStudent(Request $request) {
		$id = $request->query('id');
		$class_id = $request->query('class_id');
		
		$data = ClassroomDetail::find($id);
		$data->delete();
		return redirect()->route('showClass', ['id' => $class_id])->with('message', 'The Student successfully removed from class');		
	}
}
