<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = Teacher::get();
		return view('Teacher/index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request){
		$method = $request->method();
		if ($method == 'POST') {
			$Teacher = new Teacher();
			
			$Teacher->name = $request->input('name');
			$Teacher->email = $request->input('email');
			$Teacher->sex = $request->input('sex');
			
			$Teacher->save();
			return redirect()->route('listTeacher')->with('message', 'Data successfully inserted');
		} else {
			return view('Teacher/create');			
		}

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request){
		
		$method = $request->method();
		
		if ($method == 'POST') {
			$Teacher_id = $request->input('id');
			
			$data_update = Teacher::find($Teacher_id);

			$data_update->name = $request->input('name');
			$data_update->email = $request->input('email');
			$data_update->sex = $request->input('sex');
			
			$data_update->save();
			return redirect()->route('listTeacher')->with('message', 'Data successfully updated');			
		} else {
			$id = $request->query('id');
			$data = Teacher::find($id);
			$selected = $data->sex;
			$dropdownList = array(
				'male' => 'MALE',
				'female' => 'FEMALE'
			);
			
		}
		
		return view('Teacher/edit', compact('data', 'selected', 'dropdownList'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
		$id = $request->query('id');
		$data = Teacher::find($id);
		$data->delete();
		return redirect()->route('listTeacher')->with('message', 'Data successfully deleted');
		//$request->delete();

        //
    }
}
